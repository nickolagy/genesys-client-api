
# Genesys PGR - Java API Client

**genesys-client-api** library aims to ease integration of your existing 
accession management software in Java with [Genesys PGR portal](https://www.genesys-pgr.org "Visit Genesys PGR").

## Using the API client

### Initializing the client


	GenesysClient genesysClient = new GenesysClient();
	// Set server URL
	genesysClient.setBaseUrl("https://www.genesys-pgr.org");
	// Specify clientKey, secret and the callback URL as registered on the server
	genesysClient.connect(clientKey, clientSecret, callbackUrl);

	// Ask end user to navigate to Genesys server and authorize your access to their resources
	String authorizationUrl = genesysClient.getAuthorizationUrl(GenesysClient.EMPTY_TOKEN);
	// Open this URL in browser, login and allow us to access your resources and come back with the code
	String verifierCode = null; // whatever user provides
	genesysClient.authenticate(verifierCode);
	
	// Test it with /me
	genesysClient.me();
	
	// Tokens are now accessible. The refreshToken must be **safely** stored for future use.
	
Updating data:

	List<AccessionJson> accns=new ArrayList<AccessionJson>();
	
	// In a loop, create, fill and add entries to the list
	{
		AccessionJson accn=new AccessionJson();
		accns.add(accn);
	}
	
	// Once the appropriate batch size is reached
	// (good size is 25 or 50 entries)
	// send to server
	try {
		genesysClient.updateAccessions(instCode, accns);
	} catch (....) {
		// handle exceptions
	}


## How to reference `genesys-client-api`

The genesys-client-api snapshots are published on [OSSRH](https://oss.sonatype.org/content/groups/public).

	<dependency>
		<groupId>org.genesys-pgr</groupId>
		<artifactId>genesys-client-api</artifactId>
		<version>0.0.4-SNAPSHOT</version>
	</dependency>

### Your copy of the library

* In Eclipse, clone the  `genesys-client-api` repository into a new project
* Right-click on the `genesys-client-api` project and select Maven ... Install. 
* This will install the current version of the library into your **local** maven repository

### Your new project

* Create a new Maven project in Eclipse, if starting from scratch
* Add dependency on org.genesys-pgr:genesys-client-api artefact to your `pom.xml`

	<dependency>
		<groupId>org.genesys-pgr</groupId>
		<artifactId>genesys-client-api</artifactId>
		<version>0.0.3-SNAPSHOT</version>
	</dependency>

* Create your integration project
