/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.client.oauth.api.accession;

import org.genesys2.client.oauth.api.accession.Api1Constants.Collecting;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Class CollectingJson.
 */
public class CollectingJson {
	
	/** The coll date. */
	@JsonProperty(value = Collecting.COLLDATE)
	private String collDate;
	
	/** The coll site. */
	@JsonProperty(value = Collecting.COLLSITE)
	private String collSite;
	
	/** The coll numb. */
	@JsonProperty(value = Collecting.COLLNUMB)
	private String collNumb;
	
	/** The coll src. */
	@JsonProperty(value = Collecting.COLLSRC)
	private Integer collSrc;
	
	/** The coll code. */
	@JsonProperty(value = Collecting.COLLCODE)
	private String collCode;
	
	/** The coll name. */
	@JsonProperty(value = Collecting.COLLNAME)
	private String collName;
	
	/** The coll inst address. */
	@JsonProperty(value = Collecting.COLLINSTADDRESS)
	private String collInstAddress;
	
	/** The coll miss id. */
	@JsonProperty(value = Collecting.COLLMISSID)
	private String collMissId;

	/**
	 * Gets the coll date.
	 *
	 * @return the coll date
	 */
	public String getCollDate() {
		return collDate;
	}

	/**
	 * Sets the coll date.
	 *
	 * @param collDate the new coll date
	 */
	public void setCollDate(String collDate) {
		this.collDate = collDate;
	}

	/**
	 * Gets the coll site.
	 *
	 * @return the coll site
	 */
	public String getCollSite() {
		return collSite;
	}

	/**
	 * Sets the coll site.
	 *
	 * @param collSite the new coll site
	 */
	public void setCollSite(String collSite) {
		this.collSite = collSite;
	}

	/**
	 * Gets the coll numb.
	 *
	 * @return the coll numb
	 */
	public String getCollNumb() {
		return collNumb;
	}

	/**
	 * Sets the coll numb.
	 *
	 * @param collNumb the new coll numb
	 */
	public void setCollNumb(String collNumb) {
		this.collNumb = collNumb;
	}

	/**
	 * Gets the coll src.
	 *
	 * @return the coll src
	 */
	public Integer getCollSrc() {
		return collSrc;
	}

	/**
	 * Sets the coll src.
	 *
	 * @param integer the new coll src
	 */
	public void setCollSrc(Integer integer) {
		this.collSrc = integer;
	}

	/**
	 * Gets the coll code.
	 *
	 * @return the coll code
	 */
	public String getCollCode() {
		return collCode;
	}

	/**
	 * Sets the coll code.
	 *
	 * @param collCode the new coll code
	 */
	public void setCollCode(String collCode) {
		this.collCode = collCode;
	}

	/**
	 * Gets the coll name.
	 *
	 * @return the coll name
	 */
	public String getCollName() {
		return collName;
	}

	/**
	 * Sets the coll name.
	 *
	 * @param collName the new coll name
	 */
	public void setCollName(String collName) {
		this.collName = collName;
	}

	/**
	 * Gets the coll inst address.
	 *
	 * @return the coll inst address
	 */
	public String getCollInstAddress() {
		return collInstAddress;
	}

	/**
	 * Sets the coll inst address.
	 *
	 * @param collInstAddress the new coll inst address
	 */
	public void setCollInstAddress(String collInstAddress) {
		this.collInstAddress = collInstAddress;
	}

	/**
	 * Gets the coll miss id.
	 *
	 * @return the coll miss id
	 */
	public String getCollMissId() {
		return collMissId;
	}

	/**
	 * Sets the coll miss id.
	 *
	 * @param collMissId the new coll miss id
	 */
	public void setCollMissId(String collMissId) {
		this.collMissId = collMissId;
	}

}
