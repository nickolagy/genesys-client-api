/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.client.oauth.api;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpHeaders;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.genesys2.client.oauth.GenesysApiException;
import org.scribe.builder.api.DefaultApi20;
import org.scribe.exceptions.OAuthException;
import org.scribe.extractors.AccessTokenExtractor;
import org.scribe.extractors.JsonTokenExtractor;
import org.scribe.model.OAuthConfig;
import org.scribe.model.OAuthConstants;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.utils.OAuthEncoder;

/**
 * Genesys API v1.
 */
public class GenesysApi extends DefaultApi20 {

	/** The Constant _log. */
	private static final Logger _log = LogManager.getLogger(GenesysApi.class);

	/** OAuth2 authorization endpoint that is appended to {@link #baseUrl}. */
	public static final String AUTHORIZE_URL = "/oauth/authorize?client_id=%s&client_secret=%s&response_type=code&redirect_uri=%s";

	/** OAuth2 access token endpoint that is appended to {@link #baseUrl}. */
	private static final String TOKEN_ENDPOINT = "/oauth/token";

	/** The base URL of the Genesys server (e.g. https://www.genesys-pgr.org) */
	private String baseUrl;

	/** The complete authorization url. */
	private String authorizeUrl;

	/** The scoped authorize url. */
	private String scopedAuthorizeUrl;

	/** The access token endpoint. */
	private String accessTokenEndpoint;

	/** The refresh token endpoint. */
	private String refreshTokenEndpoint;

	/**
	 * Sets the base url.
	 *
	 * @param baseUrl the new base url
	 */
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
		this.authorizeUrl = this.baseUrl + AUTHORIZE_URL;
		this.scopedAuthorizeUrl = this.authorizeUrl + "&scope=%s";
		this.refreshTokenEndpoint = this.baseUrl + TOKEN_ENDPOINT;
		this.accessTokenEndpoint = this.baseUrl + TOKEN_ENDPOINT + "?grant_type=authorization_code";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.scribe.builder.api.DefaultApi20#getAccessTokenEndpoint()
	 */
	@Override
	public String getAccessTokenEndpoint() {
		return this.accessTokenEndpoint;
	}

	/**
	 * Gets the refresh token endpoint.
	 *
	 * @return the refresh token endpoint
	 */
	public String getRefreshTokenEndpoint() {
		return this.refreshTokenEndpoint;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.scribe.builder.api.DefaultApi20#getAuthorizationUrl(org.scribe.model
	 * .OAuthConfig)
	 */
	@Override
	public String getAuthorizationUrl(OAuthConfig config) {

		return config.hasScope() ? String.format(this.scopedAuthorizeUrl, config.getApiKey(), config.getApiSecret(),
				OAuthEncoder.encode(config.getCallback()), OAuthEncoder.encode(config.getScope())) : String
				.format(this.authorizeUrl, config.getApiKey(), config.getApiSecret(),
						OAuthEncoder.encode(config.getCallback()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.scribe.builder.api.DefaultApi20#getAccessTokenExtractor()
	 */
	@Override
	public AccessTokenExtractor getAccessTokenExtractor() {
		return new JsonTokenExtractor();
	}

	/**
	 * Gets the refresh token.
	 *
	 * @param accessToken the access token
	 * @return the refresh token
	 */
	public Token getRefreshToken(Token accessToken) {
		Pattern refreshTokenPattern = Pattern.compile("\"refresh_token\":\\s*\"(\\S*?)\"");

		Matcher matcher = refreshTokenPattern.matcher(accessToken.getRawResponse());
		if (matcher.find()) {
			return new Token(matcher.group(1), "", accessToken.getRawResponse());
		}

		return null;
	}

	/**
	 * http://stackoverflow.com/questions/20044222/spring-security-oauth-2-
	 * implicit-grant-no-support-for-refresh-token
	 * 
	 * /oauth/token?client_id=MyClient&amp;grant_type=refresh_token&amp;
	 * client_secret=
	 * mysecret&amp;refresh_token=19698a4a-960a-4d24-a8cc-44d4b71df47b
	 *
	 * @param apiKey the api key
	 * @param apiSecret the api secret
	 * @param refreshToken the refresh token
	 * @return the access token
	 * @throws GenesysApiException the genesys api exception
	 */
	public Token getAccessToken(String apiKey, String apiSecret, String refreshToken) throws GenesysApiException {
		OAuthRequest request = new OAuthRequest(getAccessTokenVerb(), this.refreshTokenEndpoint);
		request.addQuerystringParameter(OAuthConstants.CLIENT_ID, apiKey);
		request.addQuerystringParameter(OAuthConstants.CLIENT_SECRET, apiSecret);
		request.addQuerystringParameter("grant_type", "refresh_token");
		request.addQuerystringParameter("refresh_token", refreshToken);
		Response response = request.send();
		_log.debug("HTTP status code " + response.getCode());
		_log.debug("Redirect: " + response.getHeader(HttpHeaders.LOCATION));
		if (response.getCode() >= 200 && response.getCode() < 300) {
			return getAccessTokenExtractor().extract(response.getBody());
		} else if (response.getCode() == 400) {
			throw new OAuthException("Refresh token no longer valid.");
		} else {
			throw new GenesysApiException("Server responded with unexpected HTTP response code " + response.getCode());
		}
	}

	/**
	 * Gets the base url.
	 *
	 * @return the base url
	 */
	public String getBaseUrl() {
		return baseUrl;
	}

}
