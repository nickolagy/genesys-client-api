/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.client.oauth.api.accession;

import org.genesys2.client.oauth.api.accession.Api1Constants.Geo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Class GeoJson.
 */
public class GeoJson {
	
	/** The latitude. */
	@JsonProperty(value = Geo.LATITUDE)
	private Double latitude;
	
	/** The longitude. */
	@JsonProperty(value = Geo.LONGITUDE)
	private Double longitude;
	
	/** The elevation. */
	@JsonProperty(value = Geo.ELEVATION)
	private Double elevation;
	
	/** The coord uncert. */
	@JsonProperty(value = Geo.COORDUNCERT)
	private Double coordUncert;
	
	/** The coord datum. */
	@JsonProperty(value = Geo.COORDDATUM)
	private String coordDatum;
	
	/** The georef meth. */
	@JsonProperty(value = Geo.GEOREFMETH)
	private String georefMeth;

	/**
	 * Gets the latitude.
	 *
	 * @return the latitude
	 */
	public Double getLatitude() {
		return latitude;
	}

	/**
	 * Sets the latitude.
	 *
	 * @param latitude the new latitude
	 */
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	/**
	 * Gets the longitude.
	 *
	 * @return the longitude
	 */
	public Double getLongitude() {
		return longitude;
	}

	/**
	 * Sets the longitude.
	 *
	 * @param longitude the new longitude
	 */
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	/**
	 * Gets the elevation.
	 *
	 * @return the elevation
	 */
	public Double getElevation() {
		return elevation;
	}

	/**
	 * Sets the elevation.
	 *
	 * @param elevation the new elevation
	 */
	public void setElevation(Double elevation) {
		this.elevation = elevation;
	}

	/**
	 * Gets the coord uncert.
	 *
	 * @return the coord uncert
	 */
	public Double getCoordUncert() {
		return coordUncert;
	}

	/**
	 * Sets the coord uncert.
	 *
	 * @param coordUncert the new coord uncert
	 */
	public void setCoordUncert(Double coordUncert) {
		this.coordUncert = coordUncert;
	}

	/**
	 * Gets the coord datum.
	 *
	 * @return the coord datum
	 */
	public String getCoordDatum() {
		return coordDatum;
	}

	/**
	 * Sets the coord datum.
	 *
	 * @param coordDatum the new coord datum
	 */
	public void setCoordDatum(String coordDatum) {
		this.coordDatum = coordDatum;
	}

	/**
	 * Gets the georef meth.
	 *
	 * @return the georef meth
	 */
	public String getGeorefMeth() {
		return georefMeth;
	}

	/**
	 * Sets the georef meth.
	 *
	 * @param georefMeth the new georef meth
	 */
	public void setGeorefMeth(String georefMeth) {
		this.georefMeth = georefMeth;
	}

}
