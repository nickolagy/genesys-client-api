/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.client.oauth.api.accession;

import org.genesys2.client.oauth.api.accession.Api1Constants.Accession;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Class AccessionJson.
 */
public class AccessionJson {
	
	/** The version. */
	private Long version;
	
	/** The genesys id. */
	private Long genesysId;

	/** The inst code. */
	@JsonProperty(value = Accession.INSTCODE)
	private String instCode;
	
	/** The acce numb. */
	@JsonProperty(value = Accession.ACCENUMB)
	private String acceNumb;
	
	/** The new acce numb. */
	@JsonProperty(value = Accession.ACCENUMB_NEW)
	private String newAcceNumb;
	
	/** The genus. */
	@JsonProperty(value = Accession.GENUS)
	private String genus;
	
	/** The new genus. */
	@JsonProperty(value = Accession.GENUS_NEW)
	private String newGenus;
	
	/** The species. */
	@JsonProperty(value = Accession.SPECIES)
	private String species;
	
	/** The spauthor. */
	@JsonProperty(value = Accession.SPAUTHOR)
	private String spauthor;
	
	/** The subtaxa. */
	@JsonProperty(value = Accession.SUBTAXA)
	private String subtaxa;
	
	/** The subtauthor. */
	@JsonProperty(value = Accession.SUBTAUTHOR)
	private String subtauthor;
	
	/** The uuid. */
	@JsonProperty(value = Accession.UUID)
	private String uuid;
	
	/** The org cty. */
	@JsonProperty(value = Accession.ORIGCTY)
	private String orgCty;
	
	/** The acq date. */
	@JsonProperty(value = Accession.ACQDATE)
	private String acqDate;
	
	/** The mls stat. */
	@JsonProperty(value = Accession.MLSSTAT)
	private Boolean mlsStat;
	
	/** The in trust. */
	@JsonProperty(value = Accession.INTRUST)
	private Boolean inTrust;
	
	/** The available. */
	@JsonProperty(value = Accession.AVAILABLE)
	private Boolean available;
	
	/** The storage. */
	@JsonProperty(value = Accession.STORAGE)
	private Integer[] storage;
	
	/** The samp stat. */
	@JsonProperty(value = Accession.SAMPSTAT)
	private Integer sampStat;
	
	/** The dupl site. */
	@JsonProperty(value = Accession.DUPLSITE)
	private String[] duplSite;
	
	/** The bred code. */
	@JsonProperty(value = Accession.BREDCODE)
	private String bredCode;
	
	/** The ancest. */
	@JsonProperty(value = Accession.ANCEST)
	private String ancest;
	
	/** The donor code. */
	@JsonProperty(value = Accession.DONORCODE)
	private String donorCode;
	
	/** The donor numb. */
	@JsonProperty(value = Accession.DONORNUMB)
	private String donorNumb;
	
	/** The donor name. */
	@JsonProperty(value = Accession.DONORNAME)
	private String donorName;
	
	/** The coll. */
	@JsonProperty(value = Accession.COLL)
	private CollectingJson coll;
	
	/** The geo. */
	@JsonProperty(value = Accession.GEO)
	private GeoJson geo;
	
	/** The remarks. */
	@JsonProperty(value = Accession.REMARKS)
	private Remark[] remarks;
	
	/** The historic. */
	@JsonProperty(value = Accession.HISTORIC)
	private Boolean historic;

	/**
	 * Gets the version.
	 *
	 * @return the version
	 */
	public Long getVersion() {
		return version;
	}

	/**
	 * Sets the version.
	 *
	 * @param l the new version
	 */
	public void setVersion(Long l) {
		this.version = l;
	}

	/**
	 * Gets the genesys id.
	 *
	 * @return the genesys id
	 */
	public Long getGenesysId() {
		return genesysId;
	}

	/**
	 * Sets the genesys id.
	 *
	 * @param genesysId the new genesys id
	 */
	public void setGenesysId(Long genesysId) {
		this.genesysId = genesysId;
	}

	/**
	 * Gets the inst code.
	 *
	 * @return the inst code
	 */
	public String getInstCode() {
		return instCode;
	}

	/**
	 * Sets the inst code.
	 *
	 * @param instCode the new inst code
	 */
	public void setInstCode(String instCode) {
		this.instCode = instCode;
	}

	/**
	 * Gets the acce numb.
	 *
	 * @return the acce numb
	 */
	public String getAcceNumb() {
		return acceNumb;
	}

	/**
	 * Sets the acce numb.
	 *
	 * @param acceNumb the new acce numb
	 */
	public void setAcceNumb(String acceNumb) {
		this.acceNumb = acceNumb;
	}
	
	/**
	 * Gets the new acce numb.
	 *
	 * @return the new acce numb
	 */
	public String getNewAcceNumb() {
		return newAcceNumb;
	}
	
	/**
	 * Sets the new acce numb.
	 *
	 * @param newAcceNumb the new new acce numb
	 */
	public void setNewAcceNumb(String newAcceNumb) {
		this.newAcceNumb = newAcceNumb;
	}

	/**
	 * Gets the genus.
	 *
	 * @return the genus
	 */
	public String getGenus() {
		return genus;
	}

	/**
	 * Sets the genus.
	 *
	 * @param genus the new genus
	 */
	public void setGenus(String genus) {
		this.genus = genus;
	}
	
	/**
	 * Gets the new genus.
	 *
	 * @return the new genus
	 */
	public String getNewGenus() {
		return newGenus;
	}
	
	/**
	 * Sets the new genus.
	 *
	 * @param newGenus the new new genus
	 */
	public void setNewGenus(String newGenus) {
		this.newGenus = newGenus;
	}

	/**
	 * Gets the species.
	 *
	 * @return the species
	 */
	public String getSpecies() {
		return species;
	}

	/**
	 * Sets the species.
	 *
	 * @param species the new species
	 */
	public void setSpecies(String species) {
		this.species = species;
	}

	/**
	 * Gets the spauthor.
	 *
	 * @return the spauthor
	 */
	public String getSpauthor() {
		return spauthor;
	}

	/**
	 * Sets the spauthor.
	 *
	 * @param spauthor the new spauthor
	 */
	public void setSpauthor(String spauthor) {
		this.spauthor = spauthor;
	}

	/**
	 * Gets the subtaxa.
	 *
	 * @return the subtaxa
	 */
	public String getSubtaxa() {
		return subtaxa;
	}

	/**
	 * Sets the subtaxa.
	 *
	 * @param subtaxa the new subtaxa
	 */
	public void setSubtaxa(String subtaxa) {
		this.subtaxa = subtaxa;
	}

	/**
	 * Gets the subtauthor.
	 *
	 * @return the subtauthor
	 */
	public String getSubtauthor() {
		return subtauthor;
	}

	/**
	 * Sets the subtauthor.
	 *
	 * @param subtauthor the new subtauthor
	 */
	public void setSubtauthor(String subtauthor) {
		this.subtauthor = subtauthor;
	}

	/**
	 * Gets the uuid.
	 *
	 * @return the uuid
	 */
	public String getUuid() {
		return uuid;
	}

	/**
	 * Sets the uuid.
	 *
	 * @param uuid the new uuid
	 */
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	/**
	 * Gets the org cty.
	 *
	 * @return the org cty
	 */
	public String getOrgCty() {
		return orgCty;
	}

	/**
	 * Sets the org cty.
	 *
	 * @param orgCty the new org cty
	 */
	public void setOrgCty(String orgCty) {
		this.orgCty = orgCty;
	}

	/**
	 * Gets the acq date.
	 *
	 * @return the acq date
	 */
	public String getAcqDate() {
		return acqDate;
	}

	/**
	 * Sets the acq date.
	 *
	 * @param acqDate the new acq date
	 */
	public void setAcqDate(String acqDate) {
		this.acqDate = acqDate;
	}

	/**
	 * Gets the mls stat.
	 *
	 * @return the mls stat
	 */
	public Boolean getMlsStat() {
		return mlsStat;
	}

	/**
	 * Sets the mls stat.
	 *
	 * @param mlsStat the new mls stat
	 */
	public void setMlsStat(Boolean mlsStat) {
		this.mlsStat = mlsStat;
	}

	/**
	 * Gets the in trust.
	 *
	 * @return the in trust
	 */
	public Boolean getInTrust() {
		return inTrust;
	}

	/**
	 * Sets the in trust.
	 *
	 * @param inTrust the new in trust
	 */
	public void setInTrust(Boolean inTrust) {
		this.inTrust = inTrust;
	}

	/**
	 * Gets the available.
	 *
	 * @return the available
	 */
	public Boolean getAvailable() {
		return available;
	}

	/**
	 * Sets the available.
	 *
	 * @param available the new available
	 */
	public void setAvailable(Boolean available) {
		this.available = available;
	}

	/**
	 * Gets the storage.
	 *
	 * @return the storage
	 */
	public Integer[] getStorage() {
		return storage;
	}

	/**
	 * Sets the storage.
	 *
	 * @param storage the new storage
	 */
	public void setStorage(Integer[] storage) {
		this.storage = storage;
	}

	/**
	 * Gets the samp stat.
	 *
	 * @return the samp stat
	 */
	public Integer getSampStat() {
		return sampStat;
	}

	/**
	 * Sets the samp stat.
	 *
	 * @param sampStat the new samp stat
	 */
	public void setSampStat(Integer sampStat) {
		this.sampStat = sampStat;
	}

	/**
	 * Gets the dupl site.
	 *
	 * @return the dupl site
	 */
	public String[] getDuplSite() {
		return duplSite;
	}

	/**
	 * Sets the dupl site.
	 *
	 * @param duplSite the new dupl site
	 */
	public void setDuplSite(String[] duplSite) {
		this.duplSite = duplSite;
	}

	/**
	 * Gets the bred code.
	 *
	 * @return the bred code
	 */
	public String getBredCode() {
		return bredCode;
	}

	/**
	 * Sets the bred code.
	 *
	 * @param bredCode the new bred code
	 */
	public void setBredCode(String bredCode) {
		this.bredCode = bredCode;
	}

	/**
	 * Gets the ancest.
	 *
	 * @return the ancest
	 */
	public String getAncest() {
		return ancest;
	}

	/**
	 * Sets the ancest.
	 *
	 * @param ancest the new ancest
	 */
	public void setAncest(String ancest) {
		this.ancest = ancest;
	}

	/**
	 * Gets the donor code.
	 *
	 * @return the donor code
	 */
	public String getDonorCode() {
		return donorCode;
	}

	/**
	 * Sets the donor code.
	 *
	 * @param donorCode the new donor code
	 */
	public void setDonorCode(String donorCode) {
		this.donorCode = donorCode;
	}

	/**
	 * Gets the donor numb.
	 *
	 * @return the donor numb
	 */
	public String getDonorNumb() {
		return donorNumb;
	}

	/**
	 * Sets the donor numb.
	 *
	 * @param donorNumb the new donor numb
	 */
	public void setDonorNumb(String donorNumb) {
		this.donorNumb = donorNumb;
	}

	/**
	 * Gets the donor name.
	 *
	 * @return the donor name
	 */
	public String getDonorName() {
		return donorName;
	}

	/**
	 * Sets the donor name.
	 *
	 * @param donorName the new donor name
	 */
	public void setDonorName(String donorName) {
		this.donorName = donorName;
	}

	/**
	 * Gets the coll.
	 *
	 * @return the coll
	 */
	public CollectingJson getColl() {
		return coll;
	}

	/**
	 * Sets the coll.
	 *
	 * @param coll the new coll
	 */
	public void setColl(CollectingJson coll) {
		this.coll = coll;
	}

	/**
	 * Gets the geo.
	 *
	 * @return the geo
	 */
	public GeoJson getGeo() {
		return geo;
	}

	/**
	 * Sets the geo.
	 *
	 * @param geo the new geo
	 */
	public void setGeo(GeoJson geo) {
		this.geo = geo;
	}

	/**
	 * Gets the remarks.
	 *
	 * @return the remarks
	 */
	public Remark[] getRemarks() {
		return remarks;
	}

	/**
	 * Sets the remarks.
	 *
	 * @param remarks the new remarks
	 */
	public void setRemarks(Remark[] remarks) {
		this.remarks = remarks;
	}

	/**
	 * Sets the historic.
	 *
	 * @param historic the new historic
	 */
	public void setHistoric(Boolean historic) {
		this.historic = historic;
	}

	/**
	 * Gets the historic.
	 *
	 * @return the historic
	 */
	public Boolean getHistoric() {
		return this.historic;
	}
}
