/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.client.oauth;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;
import org.scribe.model.Token;

/**
 * A place to keep the tokens for the session.
 */
public class GenesysTokens implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3022586353518887500L;

	/** The access token. */
	private String accessToken = null;
	
	/** The refresh token. */
	private String refreshToken = null;

	/** The _access token. */
	private Token _accessToken = null;

	/**
	 * Gets the access token.
	 *
	 * @return the access token
	 */
	public String getAccessToken() {
		return accessToken;
	}

	/**
	 * Sets the access token.
	 *
	 * @param accessToken the new access token
	 */
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
		this._accessToken = new Token(accessToken, "");
	}

	/**
	 * Gets the refresh token.
	 *
	 * @return the refresh token
	 */
	public String getRefreshToken() {
		return refreshToken;
	}

	/**
	 * Sets the refresh token.
	 *
	 * @param refreshToken the new refresh token
	 */
	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	/**
	 * Checks for refresh token.
	 *
	 * @return true, if successful
	 */
	public boolean hasRefreshToken() {
		return StringUtils.isNotBlank(this.refreshToken);
	}

	/**
	 * Checks for access token.
	 *
	 * @return true, if successful
	 */
	public boolean hasAccessToken() {
		return StringUtils.isNotBlank(this.accessToken);
	}

	/**
	 * Access token.
	 *
	 * @return the token
	 */
	public Token accessToken() {
		return this._accessToken;
	}
}
