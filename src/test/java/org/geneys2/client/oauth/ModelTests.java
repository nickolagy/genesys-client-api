/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.geneys2.client.oauth;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;

import java.io.IOException;

import org.genesys2.client.oauth.api.accession.AccessionJson;
import org.genesys2.client.oauth.api.accession.Api1Constants.Accession;
import org.genesys2.client.oauth.api.accession.Api1Constants.Geo;
import org.genesys2.client.oauth.api.accession.GeoJson;
import org.junit.Test;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

// TODO: Auto-generated Javadoc
/**
 * The Class ModelTests.
 */
public class ModelTests {

	/** The object mapper. */
	private static ObjectMapper objectMapper;

	static {
		objectMapper = new ObjectMapper();
		objectMapper.configure(SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
		objectMapper.configure(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS, false);
		objectMapper.setSerializationInclusion(Include.NON_NULL);
	}

	/**
	 * Serialize core.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void serializeCore() throws IOException {
		AccessionJson aj = new AccessionJson();
		aj.setInstCode("INS000");
		aj.setAcceNumb("ACC-1");
		aj.setGenus("Genus");

		JsonNode tree = getJsonTree(aj);
		System.err.println(tree.toString());
		assertThat("JSON elements count mismatch", tree, JsonNodeMatchers.withSize(3));
		assertThat("No INSTCODE", tree.get(Accession.INSTCODE), notNullValue());
		assertThat("INSTCODE must be a text node", tree.get(Accession.INSTCODE), JsonNodeMatchers.isTextNode());
		assertThat("Wrong INSTCODE", tree.get(Accession.INSTCODE).asText(), is("INS000"));
		assertThat("No ACCENUMB", tree.get(Accession.ACCENUMB), notNullValue());
		assertThat("ACCENUMB must be a text node", tree.get(Accession.ACCENUMB), JsonNodeMatchers.isTextNode());
		assertThat("Wrong ACCENUMB", tree.get(Accession.ACCENUMB).asText(), is("ACC-1"));
		assertThat("No GENUS", tree.get(Accession.GENUS), notNullValue());
		assertThat("Wrong GENUS", tree.get(Accession.GENUS).asText(), is("Genus"));
	}

	/**
	 * Gets the json tree.
	 *
	 * @param o the o
	 * @return the json tree
	 * @throws JsonProcessingException the json processing exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private JsonNode getJsonTree(Object o) throws JsonProcessingException, IOException {
		return objectMapper.readTree(objectMapper.writeValueAsString(o));
	}

	/**
	 * Serialize geo.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void serializeGeo() throws IOException {
		GeoJson geo = new GeoJson();
		geo.setLatitude(1d);
		geo.setLongitude(33d);

		JsonNode tree = getJsonTree(geo);
		assertThat("JSON elements count mismatch", tree, JsonNodeMatchers.withSize(2));
		assertThat("LATITUDE must be a number", tree.get(Geo.LATITUDE), JsonNodeMatchers.isNumericNode());
		assertThat("LONGITUDE must be a number", tree.get(Geo.LONGITUDE), JsonNodeMatchers.isNumericNode());
	}

	/**
	 * Test storage.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testStorage() throws IOException {
		AccessionJson aj = new AccessionJson();
		aj.setStorage(new Integer[] { 1, 2, 3, 4 });

		JsonNode tree = getJsonTree(aj);
		System.err.println(tree.toString());
		assertThat("JSON elements count mismatch", tree, JsonNodeMatchers.withSize(1));
		assertThat("STORAGE must be an array", tree.get(Accession.STORAGE), JsonNodeMatchers.isArrayNode());
	}
}
