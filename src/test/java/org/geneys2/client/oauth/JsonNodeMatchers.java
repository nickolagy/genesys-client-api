/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.geneys2.client.oauth;

import static org.hamcrest.CoreMatchers.*;

import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.collection.IsIterableWithSize;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.NumericNode;
import com.fasterxml.jackson.databind.node.TextNode;

// TODO: Auto-generated Javadoc
/**
 * The Class JsonNodeMatchers.
 */
public class JsonNodeMatchers {

	/**
	 * With size.
	 *
	 * @param size the size
	 * @return the matcher
	 */
	@Factory
	public static Matcher<Iterable<JsonNode>> withSize(int size) {
		return IsIterableWithSize.<JsonNode> iterableWithSize(size);
	}

	/**
	 * Checks if is text node.
	 *
	 * @return the matcher<? super json node>
	 */
	@Factory
	public static Matcher<? super JsonNode> isTextNode() {
		return is(instanceOf(TextNode.class));
	}

	/**
	 * Checks if is numeric node.
	 *
	 * @return the matcher<? super json node>
	 */
	@Factory
	public static Matcher<? super JsonNode> isNumericNode() {
		return is(instanceOf(NumericNode.class));
	}

	/**
	 * Checks if is array node.
	 *
	 * @return the matcher<? super json node>
	 */
	public static Matcher<? super JsonNode> isArrayNode() {
		return is(instanceOf(ArrayNode.class));
	}
}
