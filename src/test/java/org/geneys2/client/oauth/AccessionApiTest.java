/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.geneys2.client.oauth;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.genesys2.client.oauth.GenesysApiException;
import org.genesys2.client.oauth.GenesysClient;
import org.genesys2.client.oauth.OAuthAuthenticationException;
import org.genesys2.client.oauth.PleaseRetryException;
import org.genesys2.client.oauth.api.accession.AccessionJson;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

// TODO: Auto-generated Javadoc
/**
 * The Class AccessionApiTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class AccessionApiTest {

	/** The object mapper. */
	private static ObjectMapper objectMapper;

	/** The genesys client. */
	@Mock
	protected GenesysClient genesysClient;

	/** The inst code. */
	private String instCode = "INS000";

	/** The mock server. */
	private MockGenesysServer mockServer = new MockGenesysServer();

	static {
		objectMapper = new ObjectMapper();
		objectMapper.configure(SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
		objectMapper.configure(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS, false);
		objectMapper.setSerializationInclusion(Include.NON_NULL);
	}

	/**
	 * Inits the mocks.
	 *
	 * @throws OAuthAuthenticationException the o auth authentication exception
	 * @throws PleaseRetryException the please retry exception
	 * @throws GenesysApiException the genesys api exception
	 * @throws InterruptedException the interrupted exception
	 * @throws JsonProcessingException the json processing exception
	 */
	@Before
	public void initMocks() throws OAuthAuthenticationException, PleaseRetryException, GenesysApiException,
			InterruptedException, JsonProcessingException {
		System.err.println("Initializing mocks");
		when(genesysClient.listAccessions(anyString(), anyInt(), Matchers.isNull(String.class))).then(
				mockServer.listAccessions());
		when(genesysClient.updateAccessions(anyString(), anyCollectionOf(AccessionJson.class))).then(
				mockServer.updateAccessions());
	}

	/**
	 * Test accessions0.
	 *
	 * @throws OAuthAuthenticationException the o auth authentication exception
	 * @throws PleaseRetryException the please retry exception
	 * @throws GenesysApiException the genesys api exception
	 * @throws JsonProcessingException the json processing exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testAccessions0() throws OAuthAuthenticationException, PleaseRetryException, GenesysApiException,
			JsonProcessingException, IOException {

		List<AccessionJson> results = objectMapper.readValue(genesysClient.listAccessions(instCode, 1, null),
				new TypeReference<List<AccessionJson>>() {
				});

		assertThat("Expected empty list", results.size(), is(0));
	}

	/**
	 * Test accessions1.
	 *
	 * @throws OAuthAuthenticationException the o auth authentication exception
	 * @throws PleaseRetryException the please retry exception
	 * @throws GenesysApiException the genesys api exception
	 * @throws JsonProcessingException the json processing exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testAccessions1() throws OAuthAuthenticationException, PleaseRetryException, GenesysApiException,
			JsonProcessingException, IOException {

		when(genesysClient.listAccessions(instCode, 1, null)).thenReturn(
				"[{\"instCode\":\"INS000\",\"acceNumb\":\"ACC-1\"}]");

		List<AccessionJson> results = objectMapper.readValue(genesysClient.listAccessions(instCode, 1, null),
				new TypeReference<List<AccessionJson>>() {
				});

		assertThat("Expected empty list", results.size(), is(1));
		AccessionJson acc1 = results.get(0);
		assertThat("INSTCODE doesn't match", acc1.getInstCode(), is(instCode));

		acc1.setHistoric(true);

	}

	/**
	 * Test upsert accessions1.
	 *
	 * @throws OAuthAuthenticationException the o auth authentication exception
	 * @throws PleaseRetryException the please retry exception
	 * @throws GenesysApiException the genesys api exception
	 * @throws JsonProcessingException the json processing exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws InterruptedException the interrupted exception
	 */
	@Test
	public void testUpsertAccessions1() throws OAuthAuthenticationException, PleaseRetryException, GenesysApiException,
			JsonProcessingException, IOException, InterruptedException {

		String result = genesysClient.listAccessions(instCode, 1, null);
		assertThat("Non-null result expected from genesysClient", result, notNullValue());
		List<AccessionJson> results = objectMapper.readValue(result, new TypeReference<List<AccessionJson>>() {
		});

		assertThat("Expected empty list", results.size(), is(0));

		List<AccessionJson> ajList = new ArrayList<AccessionJson>();

		for (int i = 0; i < 4; i++) {
			AccessionJson aj = new AccessionJson();
			aj.setInstCode(instCode);
			aj.setAcceNumb("ACC-" + (i + 1));
			aj.setGenus("Genus");
			ajList.add(aj);
		}

		result = genesysClient.updateAccessions(instCode, ajList);
		System.err.println(result);

		results = objectMapper.readValue(genesysClient.listAccessions(instCode, 1, null),
				new TypeReference<List<AccessionJson>>() {
				});

		assertThat("Expected list with 4 entries", results.size(), is(4));

		for (AccessionJson aj1 : results) {
			assertThat("Expected matching INSTCODE", aj1.getInstCode(), is(instCode));
			assertThat("Expected ACCENUMB starting with", aj1.getAcceNumb(), CoreMatchers.startsWith("ACC-"));
			assertThat("Expected GENUS=Genus", aj1.getGenus(), is("Genus"));
			assertThat("Expected SPECIES=null", aj1.getSpecies(), nullValue());
		}
		
		
	}

}
