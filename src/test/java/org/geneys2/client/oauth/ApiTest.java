/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.geneys2.client.oauth;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.genesys2.client.oauth.GenesysApiException;
import org.genesys2.client.oauth.GenesysClient;
import org.genesys2.client.oauth.OAuthAuthenticationException;
import org.genesys2.client.oauth.PleaseRetryException;
import org.genesys2.client.oauth.api.accession.Api1Constants.Accession;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.scribe.model.Verb;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;

// TODO: Auto-generated Javadoc
/**
 * The Class ApiTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class ApiTest {

	/** The object mapper. */
	private static ObjectMapper objectMapper;
	
	/** The Constant bananaJson. */
	private static final String bananaJson;

	/** The http get mock. */
	@Mock
	protected HttpGet httpGetMock;
	
	/** The http post mock. */
	@Mock
	protected HttpPost httpPostMock;
	
	/** The genesys client. */
	@Mock
	protected GenesysClient genesysClient;

	static {
		objectMapper = new ObjectMapper();
		objectMapper.configure(SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
		objectMapper.configure(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS, false);
		objectMapper.setSerializationInclusion(Include.NON_NULL);

		bananaJson = objectMapper.createObjectNode().put("shortName", "banana").toString();
	}

	/**
	 * Inits the o auth params.
	 */
	@BeforeClass
	public static void initOAuthParams() {

	}

	/**
	 * Inits the request mocks.
	 *
	 * @throws OAuthAuthenticationException the o auth authentication exception
	 * @throws PleaseRetryException the please retry exception
	 * @throws GenesysApiException the genesys api exception
	 */
	@Before
	public void initRequestMocks() throws OAuthAuthenticationException, PleaseRetryException, GenesysApiException {
		MockitoAnnotations.initMocks(this);
		when(httpGetMock.getMethod()).thenReturn("GET");
		when(httpPostMock.getMethod()).thenReturn("POST");

		when(genesysClient.getCrop("banana")).thenReturn(bananaJson);
	}

	/**
	 * Test mock get crop.
	 *
	 * @throws OAuthAuthenticationException the o auth authentication exception
	 * @throws PleaseRetryException the please retry exception
	 * @throws GenesysApiException the genesys api exception
	 * @throws JsonProcessingException the json processing exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testMockGetCrop() throws OAuthAuthenticationException, PleaseRetryException, GenesysApiException,
			JsonProcessingException, IOException {

		String json = genesysClient.getCrop("banana");
		assertThat("Returned banana JSON doesn't match", json, equalTo(bananaJson));

		// Must be able to read the json
		objectMapper.readTree(json);
	}

	/**
	 * Test mock update crop.
	 *
	 * @throws OAuthAuthenticationException the o auth authentication exception
	 * @throws PleaseRetryException the please retry exception
	 * @throws GenesysApiException the genesys api exception
	 * @throws JsonProcessingException the json processing exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testMockUpdateCrop() throws OAuthAuthenticationException, PleaseRetryException, GenesysApiException,
			JsonProcessingException, IOException {

		String json = genesysClient.getCrop("banana");
		assertThat("Returned banana JSON doesn't match", json, equalTo(bananaJson));

		// Must be able to read the json
		objectMapper.readTree(json);
	}

	/**
	 * Test organization members.
	 *
	 * @throws OAuthAuthenticationException the o auth authentication exception
	 * @throws PleaseRetryException the please retry exception
	 * @throws GenesysApiException the genesys api exception
	 * @throws JsonProcessingException the json processing exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testOrganizationMembers() throws OAuthAuthenticationException, PleaseRetryException,
			GenesysApiException, JsonProcessingException, IOException {
		String orgSlug = "org";
		when(genesysClient.getOrganizationMembers(orgSlug)).then(returnOrganizationMembers(orgSlug));
		String members = genesysClient.getOrganizationMembers(orgSlug);
		objectMapper.readTree(members);
		System.err.println(members);
	}

	/**
	 * Return organization members.
	 *
	 * @param orgSlug the org slug
	 * @return the answer
	 */
	private Answer<String> returnOrganizationMembers(String orgSlug) {
		return new Answer<String>() {
			@Override
			public String answer(InvocationOnMock invocation) throws Throwable {
				return "[\"ORG1\",\"ORG2\"]";
			}
		};
	}

	/**
	 * Test please retry exception.
	 *
	 * @throws OAuthAuthenticationException the o auth authentication exception
	 * @throws PleaseRetryException the please retry exception
	 * @throws GenesysApiException the genesys api exception
	 * @throws JsonProcessingException the json processing exception
	 */
	@SuppressWarnings("deprecation")
	@Test(expected = PleaseRetryException.class)
	public void testPleaseRetryException() throws OAuthAuthenticationException, PleaseRetryException,
			GenesysApiException, JsonProcessingException {

		when(genesysClient.updateMLS("INS000", null)).thenThrow(PleaseRetryException.class);
		genesysClient.updateMLS("INS000", null);
	}

	/**
	 * Test2.
	 *
	 * @throws OAuthAuthenticationException the o auth authentication exception
	 * @throws PleaseRetryException the please retry exception
	 * @throws GenesysApiException the genesys api exception
	 */
	@Test
	public void test2() throws OAuthAuthenticationException, PleaseRetryException, GenesysApiException {

		when(genesysClient.query(Mockito.isA(Verb.class), anyString(), anyMapOf(String.class, String.class), anyString())).thenCallRealMethod();

		Collection<ObjectNode> list = new ArrayList<ObjectNode>();

		list.add(objectMapper.createObjectNode().put(Accession.INSTCODE, "INS000").put(Accession.ACCENUMB, "ACC-1")
				.put(Accession.GENUS, "Genus"));
	}
}
