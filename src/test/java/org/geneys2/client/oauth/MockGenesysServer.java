/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/


package org.geneys2.client.oauth;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.genesys2.client.oauth.GenesysApiException;
import org.genesys2.client.oauth.api.accession.AccessionJson;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

// TODO: Auto-generated Javadoc
/**
 * Mock Genesys server.
 *
 * @author mobreza
 */
public class MockGenesysServer {
	
	/** The Constant BLANK_LIST. */
	private static final ArrayList<AccessionJson> BLANK_LIST = new ArrayList<AccessionJson>();
	
	/** The inst acc. */
	private final Map<String, List<AccessionJson>> instAcc = new HashMap<String, List<AccessionJson>>();

	/** The object mapper. */
	private static ObjectMapper objectMapper;

	static {
		objectMapper = new ObjectMapper();
		objectMapper.configure(SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
		objectMapper.configure(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS, false);
		objectMapper.setSerializationInclusion(Include.NON_NULL);
	}

	/**
	 * List accessions.
	 *
	 * @return the answer
	 */
	public Answer<String> listAccessions() {
		return new Answer<String>() {
			@Override
			public String answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();
				if (args[2] != null)
					throw new MockGenesysException("Querying is not supported.");
				String instCode = (String) args[0];
				int page = (Integer) args[1];

				System.err.println("Listing accessions instCode=" + instCode + " page=" + page);

				return objectMapper.writeValueAsString(getAccessionsPage(instCode, page));
			}
		};
	}

	/**
	 * Update accessions.
	 *
	 * @return the answer
	 */
	public Answer<String> updateAccessions() {
		return new Answer<String>() {
			@Override
			public String answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();
				String instCode = (String) args[0];
				List<?> arrayNode = (List<?>) args[1];

				String res = objectMapper.writeValueAsString(upsertAccessions(instCode, arrayNode));
				System.err.println("Result: " + res);
				return res;
			}
		};
	}

	/**
	 * Upsert accessions.
	 *
	 * @param instCode the inst code
	 * @param nodeList the node list
	 * @return the string
	 * @throws JsonParseException the json parse exception
	 * @throws JsonMappingException the json mapping exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws GenesysApiException the genesys api exception
	 */
	protected String upsertAccessions(final String instCode, List<?> nodeList) throws JsonParseException,
			JsonMappingException, IOException, GenesysApiException {
		List<AccessionJson> accList = instAcc.get(instCode);
		if (accList == null) {
			instAcc.put(instCode, accList = new ArrayList<AccessionJson>());
		}
		for (int i = 0; i < nodeList.size(); i++) {
			AccessionJson aj = (AccessionJson) nodeList.get(i);
			AccessionJson existing = findMatch(accList, aj);
			merge(accList, existing, aj);
		}
		return "OK";

	}

	/**
	 * Merge.
	 *
	 * @param accList the acc list
	 * @param existing the existing
	 * @param aj the aj
	 * @throws MockGenesysException the mock genesys exception
	 */
	private void merge(List<AccessionJson> accList, AccessionJson existing, AccessionJson aj)
			throws MockGenesysException {
		if (existing == null) {
			accList.add(aj);
		} else {
			if (aj.getAvailable() != null)
				existing.setAvailable(aj.getAvailable());
			if (aj.getHistoric() != null)
				existing.setHistoric(aj.getHistoric());
			if (aj.getInTrust() != null)
				existing.setInTrust(aj.getInTrust());
			if (aj.getMlsStat() != null)
				existing.setMlsStat(aj.getMlsStat());
			if (aj.getAcceNumb() != null)
				existing.setAcceNumb(aj.getAcceNumb());
			if (aj.getAcqDate() != null)
				existing.setAcqDate(aj.getAcqDate());
			if (aj.getAncest() != null)
				existing.setAncest(aj.getAncest());
			if (aj.getBredCode() != null)
				existing.setBredCode(aj.getBredCode());
			if (aj.getColl() != null) {

			}
			if (aj.getDonorCode() != null)
				existing.setDonorCode(aj.getDonorCode());
			if (aj.getDonorName() != null)
				existing.setDonorName(aj.getDonorName());
			if (aj.getDonorNumb() != null)
				existing.setDonorNumb(aj.getDonorNumb());
			if (aj.getDuplSite() != null)
				existing.setDuplSite(aj.getDuplSite());
			if (aj.getGeo() != null) {

			}
			if (aj.getOrgCty() != null)
				existing.setOrgCty(aj.getOrgCty());
			if (aj.getRemarks() != null)
				existing.setRemarks(aj.getRemarks());
			if (aj.getSampStat() != null)
				existing.setSampStat(aj.getSampStat());
			if (aj.getSpecies() != null)
				existing.setSpecies(aj.getSpecies());
			if (aj.getSpauthor() != null)
				existing.setSpauthor(aj.getSpauthor());
			if (aj.getSubtaxa() != null)
				existing.setSubtaxa(aj.getSubtaxa());
			if (aj.getSubtauthor() != null)
				existing.setSubtauthor(aj.getSubtauthor());
			if (aj.getUuid() != null) {
				if (existing.getUuid() == null) {
					existing.setUuid(aj.getUuid());
				} else if (!existing.getUuid().equals(aj.getUuid())) {
					// Can this happen?
					throw new MockGenesysException("UUID mismatch");
				}
			}
			if (aj.getAvailable() != null)
				existing.setAvailable(aj.getAvailable());
		}
	}

	/**
	 * Find match.
	 *
	 * @param accList the acc list
	 * @param aj the aj
	 * @return the accession json
	 */
	private AccessionJson findMatch(List<AccessionJson> accList, AccessionJson aj) {
		if (aj.getUuid() != null) {
			return findMatch(accList, aj.getUuid());
		} else {
			for (AccessionJson m : accList) {
				if (m.getInstCode().equals(aj.getInstCode()) && m.getAcceNumb().equals(aj.getAcceNumb())
						&& m.getGenus().equals(aj.getGenus())) {
					return m;
				}
			}
		}
		return null;
	}

	/**
	 * Find match.
	 *
	 * @param accList the acc list
	 * @param uuid the uuid
	 * @return the accession json
	 */
	private AccessionJson findMatch(List<AccessionJson> accList, String uuid) {
		for (AccessionJson m : accList) {
			if (m.getUuid().equals(uuid))
				return m;
		}
		return null;
	}

	/**
	 * Gets the accessions page.
	 *
	 * @param instCode the inst code
	 * @param page the page
	 * @return the accessions page
	 * @throws MockGenesysException the mock genesys exception
	 */
	protected List<AccessionJson> getAccessionsPage(final String instCode, final int page) throws MockGenesysException {
		List<AccessionJson> accList = instAcc.get(instCode);
		if (accList == null || accList.isEmpty()) {
			System.err.println("Returning blank list");
			return BLANK_LIST;
		}
		if (page != 1) {
			throw new MockGenesysException("page argument is not suported");
		}

		return accList;
	}

}
